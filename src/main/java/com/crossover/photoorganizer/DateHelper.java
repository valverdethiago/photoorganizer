package com.crossover.photoorganizer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by valve on 3/31/2017.
 */
public class DateHelper {

  private static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

  public static Date getDate(String dateFormat, String date) {
    try {
      return new SimpleDateFormat(dateFormat).parse(date);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static String formatDate(Date date) {
    return formatDate(DEFAULT_DATE_FORMAT, date);
  }

  public static String formatDate(String dateFormat, Date date) {
    if (date == null) {
      return null;
    }
    return new SimpleDateFormat(dateFormat).format(date);
  }

  public static LocalDate toLocalDate(Date date) {
    ZoneId defaultZoneId = ZoneId.systemDefault();
    Instant instant = date.toInstant();
    return instant.atZone(defaultZoneId).toLocalDate();
  }
}
