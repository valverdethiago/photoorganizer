package com.crossover.photoorganizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalUnit;
import java.util.Date;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FilenameUtils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

/**
 * Created by valve on 3/31/2017.
 */
public class Main {

  private static final String FILE_FOLDER_PATH = "c:\\opt\\photos";
  private static final Date BIRTH_DATE = DateHelper.getDate("dd/MM/yyyy", "19/01/2012");

  public static void main(String[] args) throws Exception {
    Files.list(Paths.get(FILE_FOLDER_PATH))
      .filter(file -> {
        String extension = FilenameUtils.getExtension(file.toFile().getName());
        return !extension.endsWith("mp4");
      })
      .forEach(file -> {
        try {
          Date creationDate = FileHelper.readCreationTime(file);
          String formatedDate = DateHelper.formatDate(creationDate);
          Period period = Period.between(DateHelper.toLocalDate(BIRTH_DATE), DateHelper.toLocalDate(creationDate));
          System.out.println(String.format("%s - %s : %dº ano, %d mês", file.toFile().getName(), formatedDate, period.getYears(), period.getMonths()));
          System.out.println(getFolder(period.getYears(), period.getMonths()));
          FileHelper.moveTo(file, getFolder(period.getYears(), period.getMonths()));
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
  }

  private static String getFolder(int years, int months) {
    if(years <0 || months < 0) {
      return FILE_FOLDER_PATH+"\\antes_bia";
    }
    String folder = String.format("%s\\%sº ano", FILE_FOLDER_PATH, years+1);
    if(years <= 2) {
      folder += "\\"+(months+1)+"º mês";
    }
    return folder;
  }
}
