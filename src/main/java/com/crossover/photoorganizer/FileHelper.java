package com.crossover.photoorganizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.StreamSupport;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

/**
 * Created by valve on 4/1/2017.
 */
public class FileHelper {



  public static Date readCreationTime(Path path) throws Exception  {
    Date creationDate = getImageDateTaken(path);
    if(creationDate == null) {
      creationDate = readCreationDateFromConsole(path);
      if(creationDate == null) {
        creationDate = readConventionalCreationTime(path);
      }
    }
    return creationDate;
  }

  private static Date readCreationDateFromConsole(Path path) throws Exception {
    Process proc =
      Runtime.getRuntime().exec(String.format("cmd /c dir %s /tc ", path.toFile().getAbsolutePath()));

    BufferedReader br =
      new BufferedReader(
        new InputStreamReader(proc.getInputStream()));

    String data = "";

    //it's quite stupid but work
    for (int i = 0; i < 6; i++) {
      data = br.readLine();
    }
    if(data == null || "".equals(data.trim())) {
      return null;
    }

    //split by space
    StringTokenizer st = new StringTokenizer(data);
    String date = st.nextToken();//Get date
    String time = st.nextToken() + " "+st.nextToken();//Get time

    return DateHelper.getDate("MM/dd/yyyy hh:mm a", date + " " + time);
  }

  private static Date getImageDateTaken(Path path) throws Exception {
    try {
      Metadata metadata = ImageMetadataReader.readMetadata(path.toFile());
      Optional<Directory> optDir = StreamSupport.stream(metadata.getDirectories().spliterator(), false)
        .filter(dir -> dir.getName().equals("Exif IFD0"))
        .findFirst();
      if (optDir.isPresent()) {
        Optional<Tag> dateTakenTag = StreamSupport.stream(metadata.getDirectories().spliterator(), false)
          .filter(dir -> dir.getName().equals("Exif IFD0"))
          .findFirst().get().getTags()
          .stream()
          .filter(tag -> tag.getTagName().equals("Date/Time"))
          .findFirst();
        if (dateTakenTag.isPresent()) {
          return DateHelper.getDate("yyyy:MM:dd HH:mm:ss", dateTakenTag.get().getDescription());
        }
      }
    }
    catch (Exception ex) {
      //TODO
    }
    return null;
  }

  private static Date readConventionalCreationTime(Path path) throws Exception  {
    return Date.from(readAttributes(path).creationTime().toInstant());
  }

  private static BasicFileAttributes readAttributes(Path path) {
    BasicFileAttributes attributes = null;
    try {
      attributes = Files.readAttributes(path, BasicFileAttributes.class);
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    return attributes;
  }

  public static void moveTo(Path file, String folderPath) {
    File folder = new File(folderPath);
    if(!folder.exists()) {
      folder.mkdirs();
    }
    File newFile = new File(folderPath+"\\"+file.getFileName());
    file.toFile().renameTo(newFile);
  }
}
